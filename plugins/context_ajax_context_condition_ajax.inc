<?php
/**
 * @file
 * Integration with the Context module
 */
class context_ajax_context_condition_ajax extends context_condition {
  function condition_values() {
    return array('ajax' => 'Ajax', 'not_ajax' => 'Not Ajax');
  }

  function execute($value) {
    foreach ($this->get_contexts($value) as $context) {
      $this->condition_met($context, $value);
    }
  }
}